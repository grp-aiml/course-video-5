# IIT KGP FOUNDATION OF AIML COURSE VIDEOS

## VIDEO SET 5

| SL# | TOPICS |
|:---:| :----- |
|  1  | IR & Web Search Part 1 |
|  2  | IR & Web Search Part 2 |
|  3  | Image & Video Analytics Part 1 |
|  4  | Image & Video Analytics Part 2 |
